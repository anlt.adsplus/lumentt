<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->get('/key',function(){
	return str_random(32);
});
// $router->group(['middleware' => 'jwt'], function () use ($router) {
//     $router->post('auth/authen',['uses'=>'AuthController@authenticate']);
// });

$router->post('authen',[
	'uses'=>'AuthController@authenticate'
]);
$router->get('auth/reg',[
	'uses'=>'AuthController@register1'
]);
//$router->get('auth/del/{id}',['use'=>'AuthController@delete']);
// $router->get('auth/showo/{id}',['use'=>'AuthController@showone']);

$router->group(['middleware' => 'jwt'],function() use ($router) {
        $router->get('showall',['uses'=>'AuthController@showall']);

        $router->get('/showone/{id}',['uses'=>'AuthController@showone']);

		$router->post('/create',['uses'=>'AuthController@create']);
        
  		$router->put('/update/{id}',['uses' => 'AuthController@update']);

  		$router->delete('/delete/{id}',['uses'=>'AuthController@delete']);        
});

// $router->group('middleware' => 'jwt', function($id)  use($router){
//     	$router -> post('/update/{id}',['uses'=>'AuthController@update']);
// });
$router->group(['prefix' => 'abc', 'middleware' => 'jwt'], function ($id) use ($router) {

});





















/* 
$router->group(['prefix' => 'api'], function () use ($router) {
    $router->post('auth/register', 'UserController@register');
    $router->post('auth/login', 'UserController@login');
    $router->get('auth/logout', 'UserController@logout');

    $router->group(['prefix' => 'user', 'as' => 'user', 'middleware' => 'jwt.auth'], function () use ($router) {
        $router->get('info', 'UserController@getUserInfo');
    });
    $router->group(['prefix' => 'posts', 'as' => 'posts', 'middleware' => 'jwt.auth'], function () use ($router) {
        $router->get('/', 'PostController@index');
        $router->post('/', 'PostController@create');
        $router->post('/{postId}', 'PostController@update');
    });
});
 */