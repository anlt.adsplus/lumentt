<?php

namespace App\Http\Controllers;

use Illuminate\Http\Database\Eloquent\Model;
use Validator;
use App\User;
use Firebase\JWT\JWT;
use Illuminate\Http\Request;
use Firebase\JWT\ExpiredException;
use Illuminate\Support\Facades\Hash;
use Laravel\Lumen\Routing\Controller as BaseController;

    class AuthController extends Controller
    {

    private $request;
    protected $user;

    public function __construct(Request $request) {
        $this->request = $request;
    }
    protected function jwt(User $user) {
        $payload = [
            'iss' => "lumen-jwt", // Issuer of the token
            'sub' => $user->id, // Subject of the token
            'iat' => time(), // Time when JWT was issued. 
            'exp' => time() + 60*60 // Expiration time
        ];
       return JWT::encode($payload, env('JWT_SECRET'));
    } 

    public function authenticate(User $user) {
        $this->validate($this->request, [
            'email'     => 'required|email',
            'password'  => 'required'
        ]);
        $user = User::where('email', $this->request->input('email'))->first();
        if (!$user) {
            return response()->json([
                'error' => 'Email does not exist.'
            ], 400);
        }
        if (Hash::check($this->request->input('password'), $user->password)) {
            return response()->json([
                'token' => $this->jwt($user)
            ], 200);
        }
        return response()->json([
            'error' => 'Email or password is wrong.'
        ], 400);
    }

    public function showall(){
        // $users = \App\User::all();
        // return response($users);
        return response()->json(User::all());


    }

    public function showone($id){
        $users = \App\User::where('id',$id)->get();
        return response()->json($users);
    }    
      
    public function create(Request $request) {
        $crea = Validator::make($request->all(),[
            'name' => 'required',
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);        
        if($crea -> fails()){
            return response()->json(['error'=>'Fail']);
        }
        $users = new User;
        $users->name = $request->get('name');
        $users->email = $request->get('email');
        $users->password = Hash::make($request->get('password'));
        $users->save();

        return response()->json(['success'=>true,'users'=>$users],200);
    }

    public function delete($id) {
        User::findOrFail($id)->delete();
        return response('Deleted successfully', 200);
    }
    public function update($id,Request $request){
        $this->validate($request,[
            'name' => 'required',
            'email' => 'required|email|max:255',
            'password' => 'required',
        ]);   
        $up = $request->input('id');
        $up  = \App\User::findOrFail($id);
        $up->name = $request->input('name');
        $up->email = $request->input('email');
        $up->password = Hash::make($request->input('password'));
        $up->save();
 
      return response()->json($up);
    }
}
?>