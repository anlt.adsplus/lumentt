<?php
namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Laravel\Lumen\Auth\Authorizable;

class User extends Model implements AuthenticatableContract, AuthorizableContract {
    use Authenticatable, Authorizable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email','password',
    ];  

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password',
    ];

    /* Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
    /**
     */
    public function getJWTIdentifier() {
        return $this->getKey();
    }
    /*
    *Return akeyvaluearray, containinganycustomclaimstobeaddedtotheJWT.
    *
    *@return array */

    public function getJWTCustomClaims() {
        return [];
    }

    public function getId(){
        return $this->id;
    }
    // public function todo(){
    //     return $this->hasMany('App\todoModel','id_todo');
    // }
    
   
}
?>